<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="/">Lestari Bicycle</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span
                class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                {{-- <li class="nav-item"><a class="nav-link active" aria-current="page" href="#!">Products</a></li>
                <li class="nav-item"><a class="nav-link" href="#!">About</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">Shop</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#!">All Products</a></li>
                        <li>
                            <hr class="dropdown-divider" />
                        </li>
                        <li><a class="dropdown-item" href="#!">Popular Items</a></li>
                        <li><a class="dropdown-item" href="#!">New Arrivals</a></li>
                    </ul>
                </li> --}}
            </ul>
            <ul class="navbar-nav mb-2 mb-lg-0 ms-lg-4">
                @guest
                    <li class="nav-item"><a class="nav-link active btn text-decoration-none text-dark" aria-current="page"
                            href="/login"><i class="bi bi-box-arrow-in-right"></i> Login</a></li>
                @else
                    <li class="nav-item"><a class="nav-link" href="#!">Cart </a></li>
                    <li class="dropdown">
                        <a class="btn dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            {{ Auth::user()->name }}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li>
                                <a class="dropdown-item d-flex justify-content-between align-items-start"
                                    href="/cart">Cart
                                    @php
                                        $count_cart = DB::table('carts')
                                            ->where('user_id', Auth::user()->id)
                                            ->get()
                                            ->count();
                                    @endphp
                                    <span class="badge rounded-pill bg-dark">{{ $count_cart }}</span></a>
                            </li>
                            <li><a class="dropdown-item" href="{{ route('my_order') }}">My Order</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li>
                                <form action="{{ route('logout') }}" method="post">
                                    @csrf
                                    <button class="dropdown-item" type="submit">Logout</button>
                                </form>
                            </li>
                        </ul>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
