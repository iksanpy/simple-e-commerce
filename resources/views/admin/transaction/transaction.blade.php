@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Transactions</h1>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!</strong> New product added.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Error!</strong> Somethink wrong.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#createProduct">
                    <i class="fa fa-plus"></i> Create new product
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered product_table" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th width="10%">Transaction ID</th>
                                <th>Name</th>
                                <th>NO. HP</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Transaction ID</th>
                                <th>Name</th>
                                <th>NO. HP</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->uuid }}</td>
                                    <td>{{ $transaction->name }}</td>
                                    <td>{{ $transaction->number }}</td>
                                    <td>{{ $transaction->transaction_status }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    {{-- modal create product --}}
    <div class="modal fade" id="createProduct" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="createProductLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createProductLabel">Create new product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('products.create') }}" enctype="multipart/form-data">
                    @csrf
                    @method('post')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="product_name">Product name</label>
                            <input type="text" name="name" class="form-control" id="product_name"
                                value="{{ old('name') }}" placeholder="Product name" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control" id="description" placeholder="Description here.." required></textarea>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <label for="description">Price</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Price" name="price"
                                            min="0" required>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">$</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                    <img class="img-preview img-fluid">
                                    <label for="description">Stock</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Stock" name="stock"
                                            min="0" required onchange="preview()">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Unit</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end modal create product --}}

    {{-- modal delete product --}}
    <div class="modal fade" id="deleteProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('products.destroy') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="id">
                        Are you sure you want to delete permanent this product ?
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end modal delete product --}}

    <script>
        // pilih element and select event
        document.querySelector('.custom-file-input').addEventListener('change', function(e) {
            // select id input file and get file name uploaded
            let fileName = document.getElementById("customFile").files[0].name;
            // get next sibling
            let nextSibling = e.target.nextElementSibling;
            // replace text to imaage name
            nextSibling.innerText = fileName;
        });
    </script>
@endsection
