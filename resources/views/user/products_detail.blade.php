@extends('layouts.user')

@section('content')
    <!-- Product section-->
    <section class="py-2">
        <div class="container px-4 px-lg-5 my-5">
            <div class="row gx-4 gx-lg-5 align-items-center">
                <div class="col-md-6"><img class="card-img-top mb-5 mb-md-0"
                        src="{{ asset('storage/images/' . $product->image) }}" height="600px" alt="..." />
                </div>
                <div class="col-md-6">
                    {{-- <div class="small mb-1">SKU: BST-498</div> --}}
                    <h1 class="display-5 fw-bolder">{{ $product->name }}</h1>
                    <div class="fs-5 mb-5">
                        {{-- <span class="text-decoration-line-through">$45.00</span> --}}
                        <span>{{ number_format($product->stock) }} Unit</span> |
                        <span>${{ number_format($product->price, 2) }}</span>
                    </div>
                    <p class="lead">{{ $product->description }}</p>
                    <form class="row row-cols-lg-auto g-3 align-items-center" method="post" action="/cart/store">
                        @csrf
                        <div class="col-12">
                            <input class="form-control text-center" name="product_id" type="hidden"
                                value="{{ $product->id }}" />
                            <input class="form-control text-center" name="qty" type="number" min="1"
                                max="{{ $product->stock }}" style="max-width: 5rem" value="1" />
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-outline-dark flex-shrink-0"><i
                                    class="bi-cart-fill me-1"></i>
                                Add To Cart</button>
                        </div>
                    </form>
                    <div class="d-flex">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Other items section-->
    <section class="py-2 bg-light">
        <div class="container px-4 px-lg-5 mt-5">
            <h2 class="fw-bolder mb-4">Other products</h2>
            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">

                @include('partials.all_products', ['products' => $products])

            </div>
        </div>
    </section>
@endsection
