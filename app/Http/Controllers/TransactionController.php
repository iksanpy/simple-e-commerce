<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionRequest;
use App\Models\Cart;
use App\Models\Products;
use App\Models\Transaction;
use App\Models\TransactionDetail;

class TransactionController extends Controller
{
    public function checkout(TransactionRequest $request)
    {
        $data = $request->except('transaction_details', 'product_qty');
        $data['user_id'] = auth()->user()->id;

        $transaction = Transaction::create($data);

        foreach ($request->transaction_details as $key => $product_id) {

            // create new transaction detail
            // add to array detail -
            $detail[] = new TransactionDetail([
                'transaction_id' => $transaction->id,
                'product_id' => $product_id,
                'qty' => $request->product_qty[$key]
            ]);

            // decrrement stock in -
            $product = Products::find($product_id);
            $product->stock = $product->stock - $request->product_qty[$key];
            $product->save();
        }

        // create multiple new transaction detail using relation -
        $transaction->details()->saveMany($detail);

        // delete product in cart -
        Cart::where('user_id', auth()->user()->id)->delete();

        return redirect(route('pay', $transaction->uuid))->with('success', 'Berhasil!');
    }

    public function pay($uuid)
    {
        $transaction = Transaction::UserLogin()->Uuid($uuid)->first();

        return view('user.pay', [
            'title' => 'Pay',
            'transaction' => $transaction
        ]);
    }

    public function myOrder()
    {
        return view('user.myorder', [
            'title' => 'My Order',
            'all' => Transaction::userLogin()->get(),
            'pending' => Transaction::userLogin()->transactionStatus('PENDING')->get(),
            'process' => Transaction::userLogin()->transactionStatus('PROCESS')->get(),
            'success' => Transaction::userLogin()->transactionStatus('SUCCESS')->get(),
            'failed' => Transaction::userLogin()->transactionStatus('FAILED')->get()
        ]);
    }

    // admin
    public function index()
    {
        // get all transaction
        $transaction = Transaction::all();

        return view('admin.transaction.transaction', [
            'title' => 'Transaction',
            'transactions' => $transaction
        ]);
    }
}
