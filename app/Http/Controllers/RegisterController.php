<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function create()
    {
        return view('auth.register', [
            "tittle" => "Register"
        ]);
    }

    public function store(Request $request)
    {
        // validation
        $request->validate([
            'name' => 'required',
            'email' => 'required|email:dns|unique:users,email',
            'password' => 'required|min:8|max:255',
            'confirm_password' => 'required|same:password'
        ]);

        // get input
        $name = $request->input('name');
        $email = $request->input('email');
        $role = 'user';
        $password = Hash::make($request->input('password'));

        // to array
        $data = [
            'name' => $name,
            'email' => $email,
            'role' => $role,
            'password' => $password
        ];

        // store to database
        User::create($data);

        return redirect(route('login'))->with('success', 'Your account now is registered, Please login!');
    }
}
