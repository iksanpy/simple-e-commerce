<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\TransactionController;
use App\Models\Transaction;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Generall
Route::middleware('block_admin')->group(function () {
    // all products
    Route::get('/', [ProductsController::class, 'index'])->name('home');

    // detail products
    Route::get('/products/detail/{product}', [ProductsController::class, 'show']);
});

// auth 
Route::middleware('auth')->group(function () {

    Route::middleware('role:admin')->prefix('admin/')->group(function () {
        // admin
        Route::get('/', AdminController::class);

        // product
        Route::get('products', [ProductsController::class, 'index'])->name('products');

        // store product
        Route::post('products/create', [ProductsController::class, 'store'])->name('products.create');

        // destroy product
        Route::post('products/delete', [ProductsController::class, 'destroy'])->name('products.destroy');

        // transaction
        Route::get('transactions', [TransactionController::class, 'index']);
    });

    Route::middleware('role:user')->group(function () {
        // all cart
        Route::get('cart', [CartController::class, 'index']);

        // add to cart
        Route::post('cart/store', [CartController::class, 'store']);

        // delete item in cart 
        Route::delete('cart/{cart}', [CartController::class, 'destroy']);

        // checkout
        Route::post('transaction/checkout', [TransactionController::class, 'checkout'])->name('checkout');

        // pay
        Route::get('pay/{transaction:uuid}', [TransactionController::class, 'pay'])->name('pay');

        // my order
        Route::get('transaction/myorder', [TransactionController::class, 'myOrder'])->name('my_order');
    });

    // logout
    Route::post('logout', LogoutController::class)->name('logout');
});


// guest
Route::middleware('guest')->group(function () {

    // login
    Route::get('login', [LoginController::class, 'create'])->name('login');

    // logic login
    Route::post('login', [LoginController::class, 'store']);

    // register
    Route::get('register', [RegisterController::class, 'create'])->name('register');

    // logic register
    Route::post('register', [RegisterController::class, 'store']);
});
