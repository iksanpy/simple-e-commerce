@extends('layouts.user')

@section('content')
    <section>
        <div class="container px-4 px-lg-5 my-5" style="min-height: 400px">
            <h2 class="my-3">My Order</h2>
            <div class="row gx-4 gx-lg-5">
                <div class="col-12">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-all-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-all"
                                aria-selected="true">All
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-pending-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-pending" type="button" role="tab" aria-controls="pills-pending"
                                aria-selected="false">PENDING</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-process-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-process" type="button" role="tab" aria-controls="pills-process"
                                aria-selected="false">PROCESS</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-success-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-success" type="button" role="tab" aria-controls="pills-success"
                                aria-selected="false">SUCCESS</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-failed-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-failed" type="button" role="tab" aria-controls="pills-failed"
                                aria-selected="false">FAILED</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-all" role="tabpanel"
                            aria-labelledby="pills-all-tab" tabindex="0">

                            <table class="table align-middle table-bordered text-center mb-4">
                                <tr>
                                    <td>#</td>
                                    <td>ID Transaction</td>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Grand Total</td>
                                    <td>Status</td>
                                    <td>Action</td>
                                </tr>
                                @forelse ($all as $key=>$item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->uuid }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>${{ $item->transaction_total }}</td>
                                        @if ($item->transaction_status == 'PENDING')
                                            <td><span class="badge bg-secondary">{{ $item->transaction_status }}</span>
                                            </td>
                                        @elseif($item->transaction_status == 'PROCESS')
                                            <td><span class="badge bg-warning">{{ $item->transaction_status }}</span>
                                            </td>
                                        @elseif($item->transaction_status == 'SUCCESS')
                                            <td><span class="badge bg-success">{{ $item->transaction_status }}</span>
                                            </td>
                                        @elseif($item->transaction_status == 'FAILED')
                                            <td><span class="badge bg-danger">{{ $item->transaction_status }}</span>
                                            </td>
                                        @else
                                            <td>{{ $item->transaction_status }}</td>
                                        @endif
                                        <td><button type="button" class="btn btn-primary btn-sm"><i
                                                    class="bi bi-info"></i></button></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">You haven't ordered</td>
                                    </tr>
                                @endforelse
                            </table>
                        </div>
                        <div class="tab-pane fade" id="pills-pending" role="tabpanel" aria-labelledby="pills-pending-tab"
                            tabindex="0">
                            <table class="table align-middle table-bordered text-center mb-4">
                                <tr>
                                    <td>#</td>
                                    <td>ID Transaction</td>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Grand Total</td>
                                    <td>Action</td>
                                </tr>
                                @forelse ($pending as $key=>$item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->uuid }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>${{ $item->transaction_total }}</td>
                                        <td><button type="button" class="btn btn-primary btn-sm"><i
                                                    class="bi bi-info"></i></button></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">You haven't ordered</td>
                                    </tr>
                                @endforelse
                            </table>
                        </div>
                        <div class="tab-pane fade" id="pills-process" role="tabpanel" aria-labelledby="pills-process-tab"
                            tabindex="0">
                            <table class="table align-middle table-bordered text-center mb-4">
                                <tr>
                                    <td>#</td>
                                    <td>ID Transaction</td>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Grand Total</td>
                                    <td>Action</td>
                                </tr>
                                @forelse ($process as $key=>$item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->uuid }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>${{ $item->transaction_total }}</td>
                                        <td><button type="button" class="btn btn-primary btn-sm"><i
                                                    class="bi bi-info"></i></button></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">You haven't ordered</td>
                                    </tr>
                                @endforelse
                            </table>
                        </div>
                        <div class="tab-pane fade" id="pills-success" role="tabpanel" aria-labelledby="pills-success-tab"
                            tabindex="0">
                            <table class="table align-middle table-bordered text-center mb-4">
                                <tr>
                                    <td>#</td>
                                    <td>ID Transaction</td>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Grand Total</td>
                                    <td>Action</td>
                                </tr>
                                @forelse ($success as $key=>$item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->uuid }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>${{ $item->transaction_total }}</td>
                                        <td><button type="button" class="btn btn-primary btn-sm"><i
                                                    class="bi bi-info"></i></button></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">You haven't ordered</td>
                                    </tr>
                                @endforelse
                            </table>
                        </div>
                        <div class="tab-pane fade" id="pills-failed" role="tabpanel" aria-labelledby="pills-failed-tab"
                            tabindex="0">
                            <table class="table align-middle table-bordered text-center mb-4">
                                <tr>
                                    <td>#</td>
                                    <td>ID Transaction</td>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Grand Total</td>
                                    <td>Action</td>
                                </tr>
                                @forelse ($failed as $key=>$item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->uuid }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>${{ $item->transaction_total }}</td>
                                        <td><button type="button" class="btn btn-primary btn-sm"><i
                                                    class="bi bi-info"></i></button></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">You haven't ordered</td>
                                    </tr>
                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
