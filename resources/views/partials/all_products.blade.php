@foreach ($products as $product)
    <div class="col mb-5">
        <div class="card h-100">
            <!-- Product image-->
            <img class="card-img-top" src="{{ asset('storage/images/' . $product->image) }}" alt="..."
                height="150px" />
            <!-- Product details-->
            <div class="card-body p-4">
                <div class="text-center">
                    <!-- Product name-->
                    <h5 class="fw-bolder">{{ $product->name }}</h5>
                    <!-- Product price-->
                    ${{ number_format($product->price, 2) }}
                </div>
            </div>
            <!-- Product actions-->
            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                <div class="text-center">
                    {{-- <a class="btn btn-sm btn-outline-dark mt-auto" href="#">Buy</a> --}}
                    @if ($product->stock < 1)
                        <a class="btn btn-sm btn-outline-danger mt-auto" href="#">Out Of Stock</a>
                    @else
                        <a class="btn btn-sm btn-outline-dark mt-auto" href="/products/detail/{{ $product->id }}">View
                            Detail</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach
