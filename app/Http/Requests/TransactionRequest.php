<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'number' => 'required|max:255',
            'address' => 'required',
            'transaction_total' => 'required|numeric',
            'transaction_status' => ['required', Rule::in(['PENDING', 'SUCCESS', 'FAILED'])],
            'transaction_details' => 'required|array',
            'transaction_details.*' => 'integer|exists:products,id'
        ];
    }
}
