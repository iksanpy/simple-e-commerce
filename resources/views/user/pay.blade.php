@extends('layouts.user')

@section('content')
    <section>
        <div class="container px-4 px-lg-5 my-5" style="min-height: 400px">
            <h2 class="my-3">Pay</h2>
            <div class="row gx-4 gx-lg-5">
                <div class="col-12">

                    <table class="table align-middle table-bordered text-center mb-4">
                        <tr>
                            <td>ID Transaction</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Grand Total</td>
                            <td>Status</td>
                        </tr>
                        <tr>
                            <td>{{ $transaction->uuid }}</td>
                            <td>{{ $transaction->name }}</td>
                            <td>{{ $transaction->email }}</td>
                            <td>${{ $transaction->transaction_total }}</td>
                            <td>{{ $transaction->transaction_status }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
