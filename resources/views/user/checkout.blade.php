@extends('layouts.user')

@section('content')
    <!-- Product section-->
    <section class="py-5">
        <div class="container px-4 px-lg-5 my-5">
            <form method="post">
                <div class="row gx-4 gx-lg-5">
                    @csrf
                    @method('post')
                    <div class="col-7">
                        <table class="table align-middle table-bordered text-center mb-4">
                            <tr>
                                <td>Image</td>
                                <td>Name</td>
                                <td>Units</td>
                                <td>Price</td>
                            </tr>
                            <tr>
                                <td><img src="{{ asset('storage/images/' . $product->image) }}" width="80" height="80"
                                        class="rounded" alt="">
                                </td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $qty }} Units</td>
                                <td>${{ number_format($product->price, 2) }}</td>
                            </tr>
                            {{-- <tr>
                            <td colspan="3">Total</td>
                            <td>${{ number_format($product->price * $qty, 2) }}</td>
                        </tr> --}}
                        </table>

                        <h4 class="mb-4">Buyer Information :</h4>

                        <div class="mb-3">
                            <label for="fullname" class="form-label">Full Name</label>
                            <input type="text" class="form-control" id="fullname" name="fullname"
                                placeholder="Full name here.." value="{{ Auth::user()->name }}">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email Address</label>
                            <input type="email" class="form-control" id="email" name="email"
                                placeholder="name@example.com" value="{{ Auth::user()->email }}">
                        </div>
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">Number Phone</label>
                            <input type="email" class="form-control" id="no_hp" name="no_hp" placeholder="098912xxx">
                        </div>
                        <div class="mb-3">
                            <label for="address" class="form-label">Address</label>
                            <textarea class="form-control" id="address" name="address" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="card">
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                    ID Transaction
                                    <span class="fw-bold">#{{ $id = uniqid() }}</span>
                                    <input type="hidden" name="id" value="{{ $id }}">
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                    Subtotal
                                    <span class="fw-bold">${{ number_format($product->price * $qty, 2) }}</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                    Tax
                                    <span class="fw-bold">10%</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                    Grand Total
                                    <span
                                        class="fw-bold">${{ number_format($product->price * $qty + ($product->price * $qty) / 100, 2) }}</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                    Bank Transfer
                                    <span class="fw-bold">BRI Syari'ah</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                    No Rekening
                                    <span class="fw-bold">1290 0931 1293</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                    Recipient's Name
                                    <span class="fw-bold">Ahmad Iksan</span>
                                </li>
                                <button type="submit" class="list-group-item py-3 btn btn-dark bg-dark text-white"><span
                                        class="fs-6">I Already
                                        Paid</span></button>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
