@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Products</h1>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!</strong> New product added.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Error!</strong> Somethink wrong.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#createProduct">
                    <i class="fa fa-plus"></i> Create new product
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered product_table" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th width="18%" class="text-center">Image</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Stock</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th class="text-center">Image</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Stock</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td class="text-center">
                                        <img src="{{ asset('storage/images/' . $product->image) }}" width="80" height="80"
                                            class="rounded" alt="">
                                    </td>
                                    <td>{{ $product->name }}</td>
                                    <td>${{ number_format($product->price, 2) }}</td>
                                    <td>{{ $product->stock }} Unit</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-info btn-detail-product"
                                            data-toggle="modal" data-target="#detailProduct{{ $product->id }}">
                                            <i class="fa fa-info-circle"></i>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-warning btn-edit-product"
                                            data-toggle="modal" data-target="#editProduct{{ $product->id }}">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-danger btn-delete-product"
                                            data-id_product="{{ $product->id }}" data-toggle="modal"
                                            data-target="#deleteProduct">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>

                                {{-- modal detail product --}}
                                <div class="modal fade" id="detailProduct{{ $product->id }}" data-backdrop="static"
                                    data-keyboard="false" tabindex="-1" aria-labelledby="detailProductLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="detailProductLabel">Detail product</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>Image Product</label>
                                                    <img src="{{ asset('storage/images/' . $product->image) }}"
                                                        width="400" class="d-block mx-auto img-fluid" alt="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="product_name">Product name</label>
                                                    <input type="text" name="name" class="form-control-plaintext" readonly
                                                        id="product_name" value="{{ $product->name }}"
                                                        placeholder="Product name" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="description">Description</label>
                                                    <textarea name="description" class="form-control-plaintext"
                                                        id="description" placeholder="Description here.."
                                                        required>{{ $product->description }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <label for="description">Price</label>
                                                            <div class="input-group">
                                                                <input type="text" readonly class="form-control-plaintext"
                                                                    placeholder="Price" name="price"
                                                                    value="{{ $product->price }} $" required>
                                                            </div>
                                                        </div>

                                                        <div class="col">
                                                            <label for="description">Stock</label>
                                                            <div class="input-group">
                                                                <input type="text" readonly class="form-control-plaintext"
                                                                    placeholder="Stock" name="stock"
                                                                    value="{{ $product->stock }} Unit" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- end modal detail product --}}


                                {{-- modal edit product --}}
                                <div class="modal fade" id="editProduct{{ $product->id }}" data-backdrop="static"
                                    data-keyboard="false" tabindex="-1" aria-labelledby="editProductLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="editProductLabel">Edit product</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" action="products/edit">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="product_name">Product name</label>
                                                        <input type="text" name="name" class="form-control"
                                                            id="product_name" value="{{ old('name', $product->name) }}"
                                                            placeholder="Product name" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="description">Description</label>
                                                        <textarea name="description" class="form-control" id="description"
                                                            placeholder="Description here.."
                                                            required>{{ old('description', $product->description) }}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <label for="description">Price</label>
                                                                <div class="input-group">
                                                                    <input type="number" class="form-control"
                                                                        placeholder="Price" name="price" min="0"
                                                                        value="{{ old('price', $product->price) }}"
                                                                        required>
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"
                                                                            id="basic-addon1">$</span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col">
                                                                <label for="description">Stock</label>
                                                                <div class="input-group">
                                                                    <input type="number" class="form-control"
                                                                        placeholder="Stock" name="stock" min="0"
                                                                        value="{{ old('stock', $product->stock) }}"
                                                                        required>
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"
                                                                            id="basic-addon1">Unit</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Image</label>
                                                        <div class="custom-file">
                                                            <input type="file" name="image" class="custom-file-input"
                                                                id="customFile">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                {{-- end modal edit product --}}
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    {{-- modal create product --}}
    <div class="modal fade" id="createProduct" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="createProductLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createProductLabel">Create new product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('products.create') }}" enctype="multipart/form-data">
                    @csrf
                    @method('post')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="product_name">Product name</label>
                            <input type="text" name="name" class="form-control" id="product_name"
                                value="{{ old('name') }}" placeholder="Product name" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control" id="description"
                                placeholder="Description here.." required></textarea>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <label for="description">Price</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Price" name="price" min="0"
                                            required>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">$</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                    <img class="img-preview img-fluid">
                                    <label for="description">Stock</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Stock" name="stock" min="0"
                                            required onchange="preview()">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Unit</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end modal create product --}}

    {{-- modal delete product --}}
    <div class="modal fade" id="deleteProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('products.destroy') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="id">
                        Are you sure you want to delete permanent this product ?
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end modal delete product --}}

    <script>
        // pilih element and select event
        document.querySelector('.custom-file-input').addEventListener('change', function(e) {
            // select id input file and get file name uploaded
            let fileName = document.getElementById("customFile").files[0].name;
            // get next sibling
            let nextSibling = e.target.nextElementSibling;
            // replace text to imaage name
            nextSibling.innerText = fileName;
        });
    </script>
@endsection
