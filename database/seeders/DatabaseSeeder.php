<?php

namespace Database\Seeders;

use App\Models\Products;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(5)->create();
        $this->call(
            AkunSeeder::class
        );

        Products::create([
            "name" => "Argon Series 1",
            "image" => "image1.jpg",
            "description" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum doloribus at sed architecto eum! Quaerat totam rem voluptate, labore voluptatum error a debitis architecto eos placeat natus possimus asperiores ipsa libero. Illum tempore ab ullam expedita, error natus pariatur velit enim laboriosam quisquam modi aliquam. Iste officia unde laudantium ad fugit animi nihil eveniet ab aut laborum, itaque consectetur neque odio fugiat incidunt molestiae quidem adipisci earum veritatis voluptatibus distinctio.",
            "price" => 12,
            "stock" => 12,
        ]);

        Products::create([
            "name" => "Polygon E-12",
            "image" => "image2.jpg",
            "description" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum doloribus at sed architecto eum! Quaerat totam rem voluptate, labore voluptatum error a debitis architecto eos placeat natus possimus asperiores ipsa libero. Illum tempore ab ullam expedita, error natus pariatur velit enim laboriosam quisquam modi aliquam. Iste officia unde laudantium ad fugit animi nihil eveniet ab aut laborum, itaque consectetur neque odio fugiat incidunt molestiae quidem adipisci earum veritatis voluptatibus distinctio.",
            "price" => 31,
            "stock" => 142,
        ]);

        Products::create([
            "name" => "United Underground",
            "image" => "image3.jpg",
            "description" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum doloribus at sed architecto eum! Quaerat totam rem voluptate, labore voluptatum error a debitis architecto eos placeat natus possimus asperiores ipsa libero. Illum tempore ab ullam expedita, error natus pariatur velit enim laboriosam quisquam modi aliquam. Iste officia unde laudantium ad fugit animi nihil eveniet ab aut laborum, itaque consectetur neque odio fugiat incidunt molestiae quidem adipisci earum veritatis voluptatibus distinctio.",
            "price" => 56,
            "stock" => 23,
        ]);

        Products::create([
            "name" => "Evergren Promax",
            "image" => "image4.jpg",
            "description" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum doloribus at sed architecto eum! Quaerat totam rem voluptate, labore voluptatum error a debitis architecto eos placeat natus possimus asperiores ipsa libero. Illum tempore ab ullam expedita, error natus pariatur velit enim laboriosam quisquam modi aliquam. Iste officia unde laudantium ad fugit animi nihil eveniet ab aut laborum, itaque consectetur neque odio fugiat incidunt molestiae quidem adipisci earum veritatis voluptatibus distinctio.",
            "price" => 15,
            "stock" => 34,
        ]);
    }
}
