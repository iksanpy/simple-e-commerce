<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{

    public function index()
    {

        $products = Products::latest()->get();

        if (Auth::check() && Auth::user()->role == 'admin' && request()->segment(1) == 'admin') :
            return view('admin.products.products', [
                "tittle" => "Products",
                "products" => $products
            ]);
        else :
            return view('user.products', [
                "tittle" => "Products",
                "products" => $products
            ]);
        endif;
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif|max:2048'
        ]);

        // image
        $image = $request->file('image');

        // get input
        $name = $request->input('name');
        $description = $request->input('description');
        $image_name = $image->hashName();
        $price = $request->input('price');
        $stock = $request->input('stock');

        // upload
        $image->store('images');

        if ($image->isValid()) :
            $data = [
                'name' => $name,
                'description' => $description,
                'image' => $image_name,
                'price' => $price,
                'stock' => $stock
            ];

            $product = Products::create($data);

            return redirect(route('products'))->with('success', 'New product has been added!');

        endif;

        return "Fail";
    }

    public function show(Products $product)
    {
        return view('user.products_detail', [
            "tittle" => "Products Detail",
            "product" => $product,
            "products" => Products::limit(4)->latest()->get()
        ]);
    }

    public function destroy(Request $request)
    {
        // get input
        $id = $request->input('id');

        // find
        $product = Products::find($id);

        // delete
        $product->delete();

        return redirect(route('products'))->with('success', 'Sukses');
    }

    public function checkout()
    {
        return view('user.checkout', [
            "tittle" => "Checkout",
            "product" => Products::find(request()->id),
            "qty" => request()->qty
        ]);
    }
}
