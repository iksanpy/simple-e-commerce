<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    //parsinta tutorial 
    public function create()
    {
        return view('auth.login', [
            "tittle" => "Login"
        ]);
    }

    public function store(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if ($user->role == 'admin') {
                return redirect()->intended('/admin')->with('success', 'You are now logged in');
            } else {
                return redirect()->intended('/')->with('success', 'You are now logged in');
            }

            return redirect('/');
        }

        return redirect('/login')->with('error', 'Email or password false');
    }
}
