<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    public function index()
    {
        return view('user.cart', [
            'tittle' => 'Cart',
            'carts' => Cart::where('user_id', Auth::user()->id)
                ->get(),
        ]);
    }

    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $product_id = $request->input('product_id');
        $qty = $request->input('qty');

        $data = [
            'user_id' => $user_id,
            'product_id' => $product_id,
            'qty' => $qty
        ];

        Cart::create($data);

        return redirect()->back();
    }

    public function destroy(Cart $cart)
    {
        $id_user = Auth::user()->id;
        $cart_id = $cart->id;

        $cart = Cart::where('id', $cart_id)
            ->where('user_id', $id_user);

        $cart->delete();

        return redirect('/cart')->with('success', 'Item deleted!');
    }
}
