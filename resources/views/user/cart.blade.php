@extends('layouts.user')

@section('content')
    <!-- Product section-->
    <section>
        <div class="container px-4 px-lg-5 my-5">
            <h2 class="my-3">Cart</h2>
            <div class="row gx-4 gx-lg-5">
                <div class="col-8">
                    <table class="table align-middle table-bordered text-center mb-4">
                        <tr>
                            <td>Action</td>
                            <td>Image</td>
                            <td>Name</td>
                            <td>Units</td>
                            <td>Price</td>
                        </tr>
                        @php
                            $subtotal = 0;
                            $cart_product_id = [];
                            $cart_product_qty = [];
                        @endphp
                        @forelse ($carts as $cart)
                            <tr>
                                <td>
                                    <form action="/cart/{{ $cart->id }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn">X</button>
                                    </form>
                                </td>
                                <td><img src="{{ asset('storage/images/' . $cart->product->image) }}" width="80"
                                        height="80" class="rounded" alt=""></td>
                                <td>{{ $cart->product->name }}</td>
                                <td>{{ $cart->qty }} Units</td>
                                <td>${{ $cart->product->price }}</td>
                            </tr>
                            @php
                                $subtotal += $cart->qty * $cart->product->price;
                                $cart_product_id[] = $cart->product->id;
                                $cart_product_qty[] = $cart->qty;
                            @endphp
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">No Products</td>
                            </tr>
                        @endforelse
                    </table>

                    <h4 class="mb-4">Buyer Information :</h4>


                    <form method="post" action="{{ route('checkout') }}">

                        <div class="mb-3">
                            <label for="name" class="form-label">Full Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                name="name" placeholder="Full name here.." value="{{ Auth::user()->name }}">
                            @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email Address</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                                name="email" placeholder="name@example.com" value="{{ Auth::user()->email }}">
                            @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="number" class="form-label">Number Phone</label>
                            <input type="text" class="form-control @error('number') is-invalid @enderror" id="number"
                                name="number" placeholder="098912xxx">
                            @error('number')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="address" class="form-label">Address</label>
                            <textarea class="form-control @error('address') is-invalid @enderror" id="address" name="address" rows="3"></textarea>
                            @error('address')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                </div>
                <div class="col-4">

                    @csrf
                    @method('post')
                    <div class="card">
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                ID Transaction
                                <span class="fw-bold">#{{ $id = uniqid() }}</span>
                                <input type="hidden" name="uuid" value="{{ $id }}">
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                Subtotal
                                <span class="fw-bold">${{ $subtotal }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                Tax
                                <span class="fw-bold">11%</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                Grand Total
                                @php
                                    $grandtotal = round($subtotal + $subtotal * (11 / 100), 1);
                                @endphp
                                <span class="fw-bold">${{ $grandtotal }}</span>
                                <input type="hidden" name="transaction_total" value="{{ $grandtotal }}">
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                Bank Transfer
                                <span class="fw-bold">BRI Syari'ah</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                No Rekening
                                <span class="fw-bold">1290 0931 1293</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center  py-3">
                                Recipient's Name
                                <span class="fw-bold">Ahmad Iksan</span>
                            </li>
                            <input type="hidden" name="transaction_status" value="PENDING">
                            @foreach ($cart_product_id as $crprid)
                                <input type="hidden" name="transaction_details[]" value="{{ $crprid }}">
                            @endforeach
                            @foreach ($cart_product_qty as $crprqty)
                                <input type="hidden" name="product_qty[]" value="{{ $crprqty }}">
                            @endforeach
                            <button type="submit" class="list-group-item py-3 bg-dark text-white"
                                @if ($carts->count() < 1) style="pointer-events: none;background-color: #6c757d;border-color: #6c757d;opacity: .65;" @endif>
                                <span class="fs-6">Checkout</span></button>
                        </ul>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </section>
@endsection
