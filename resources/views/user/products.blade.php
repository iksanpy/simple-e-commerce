@extends('layouts.user')

@section('content')
    <!-- Header-->
    <header class="bg-dark py-5"
        style="background-size: 100%;
                                background-repeat: no-repeat;
                                background-position-y: -200px;
                                background-blend-mode: overlay;
                                background-image: url(https://images.unsplash.com/photo-1570679334008-c97544c8695b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1529&q=80);">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-center text-white">
                <h1 class="display-4 fw-bolder">Products</h1>
                <p class="lead fw-normal text-white-50 mb-0">The best product we have.</p>
            </div>
        </div>
    </header>
    <!-- Section-->
    <section class="py-5">
        <div class="container px-4 px-lg-5 mt-5">
            @if (count($products) > 0)
                <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                    @include('partials.all_products', ['products' => $products])
                </div>
            @else
                <p class="text-center">Sorry, this product is not available at this time</p>
            @endif
        </div>
    </section>
@endsection
