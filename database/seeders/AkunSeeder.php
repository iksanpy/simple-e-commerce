<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Ini Admin',
                'email' => 'admin@gmail.com',
                'role' => 'admin',
                'password' => Hash::make('12345')
            ],
            [
                'name' => 'Ini User',
                'email' => 'user@gmail.com',
                'role' => 'user',
                'password' => Hash::make('12345')
            ]
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
